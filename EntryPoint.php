<?php
	include("ConfigParser.php");
	include("CsvFile.php");

	$parser = new ConfigParser();
	$parser->parse('myConfig.txt');
	$files = $parser->readFiles();

	foreach ($files as $file) {
		$file->sort('State');
		$file->write();
	}
?>
